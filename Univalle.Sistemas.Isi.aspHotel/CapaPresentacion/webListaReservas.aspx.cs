﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;

namespace CapaPresentacion
{
    public partial class webListaReservas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Si el usuario intenta navegar sin haberse logeado, es redireccionado a la ventana login
            if (Usuario.idActual == -1)
                Response.Redirect("webLogin.aspx");
            else
                idLogout.InnerText = Usuario.nombreActual + " (Logout)";

            if (!IsPostBack)
            {
                LoadData();
            }
        }

        public void LoadData()
        {
            try
            {
                if(Usuario.rolActual == "Admin") //si es administrador veras todas las reservas
                    gvDatos.DataSource = ReservaBRL.Select().DefaultView;
                else
                    gvDatos.DataSource = ReservaBRL.SelectByUserId(Usuario.idActual).DefaultView;
                gvDatos.DataBind();
                gvDatos.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
                //txtCadena.Value = ex.Message;
            }
        }

        protected void gvDatos_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "RETIRAR")
            {
                int index = Convert.ToInt32(e.CommandArgument.ToString());
                ReservaBRL.Delete(int.Parse(gvDatos.Rows[index].Cells[1].Text));
                Response.Redirect("webListaReservas.aspx");
            }
        }

        protected void btnVolverAlMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("webPrincipal.aspx");
        }
    }
}