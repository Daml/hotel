﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webRegistroUsuario.aspx.cs" EnableEventValidation="false" Inherits="CapaPresentacion.webRegistroUsuario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Gestion de Usuarios</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>


    <link href="css/bootstrap-grid.css" rel="stylesheet"/>
    <link href="css/bootstrap-grid.min.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.min.css" rel="stylesheet"/>

    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/ai.0.22.9-build00167.js"></script>
    <script src="scripts/ai.0.22.9-build00167.min.js"></script>
    <script src="scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <main id="main">
            <div class="container">
                
                <div class="text-right">
                    <a href="webLogin.aspx" id="idLogout" runat="server"></a>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <legend class="text-center header">Registro de usuario</legend>
                        <style>
                            .header {
                                color: #36A0FF;
                                font-size: 27px;
                                padding: 10px;
                            }
                        </style>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-8">
                                <input id="txtNombreUsuario" runat="server" name="name" type="text" placeholder="<%$ Resources:Resource, NombreUsuario %>" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-8">
                                <input id="txtNombres" runat="server" name="name" type="text" placeholder="<%$ Resources:Resource, Nombres %>" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-8">
                                <input id="txtPrimerApellido" runat="server" name="name" type="text" placeholder="<%$ Resources:Resource, PrimerApellido %>" class="form-control"/>
                            </div>
                        </div>
                                
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-8">
                                <input id="txtSegundoApellido" runat="server" name="name" type="text" placeholder="<%$ Resources:Resource, SegundoApellido %>" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-8">
                                <input id="txtContraseña" runat="server" name="name" type="password" placeholder="<%$ Resources:Resource, Contrasenha %>" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-8">
                                <input id="txtContraseñaRepeticion" runat="server" name="name" type="password" placeholder="<%$ Resources:Resource, RepetirContrasenha %>" class="form-control"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                                    
                            <div class="col-md-12 text-center">
                                <asp:Button ID="btnRegistrar" OnClick="btnRegistrar_Click"  runat="server" Text="<%$ Resources:Resource, Registrar %>"  class="btn btn-primary btn-lg"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-12 text-center">
                                <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </form>
</body>
</html>
