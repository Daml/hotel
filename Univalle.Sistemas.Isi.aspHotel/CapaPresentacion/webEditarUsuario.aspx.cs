﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;

namespace CapaPresentacion
{
    public partial class webEditarUsuario : System.Web.UI.Page
    {
        Usuario usuario;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Si el usuario intenta navegar sin haberse logeado, es redireccionado a la ventana login
            if (Usuario.idActual == -1)
                Response.Redirect("webLogin.aspx");
            else
                idLogout.InnerText = Usuario.nombreActual + " (Logout)";

            if (!IsPostBack)
            {
                usuario = UsuarioBRL.getUsuario(Usuario.editingdId);
                txtNombres.Value = usuario.Nombres;
                txtNombreUsuario.Value = usuario.NombreUsuario;
                txtPrimerApellido.Value = usuario.PrimerApellido;
                txtSegundoApellido.Value = usuario.SegundoApellido;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                usuario = new Usuario(Usuario.editingdId, txtNombreUsuario.Value, txtNombres.Value, txtPrimerApellido.Value, txtSegundoApellido.Value);
                UsuarioBRL.Update(usuario);

            }
            catch (Exception ex)
            {

                throw ex;
            }

            Response.Redirect("webListaUsuarios.aspx");
        }
    }
}