﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;

namespace CapaPresentacion
{
    public partial class webListaPromocion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Si el usuario intenta navegar sin haberse logeado, es redireccionado a la ventana login
            if (Usuario.idActual == -1)
                Response.Redirect("webLogin.aspx");
            else
                idLogout.InnerText = Usuario.nombreActual + " (Logout)";

            if (!IsPostBack)
            {
                LoadData();
            }
        }

        public void LoadData()
        {
            try
            {
                gvDatos.DataSource = PromocionBRL.Select().DefaultView;
                gvDatos.DataBind();
                gvDatos.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
                //txtCadena.Value = ex.Message;
            }
        }

        protected void gvDatos_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "ELIMINAR")
            {
                int index = Convert.ToInt32(e.CommandArgument.ToString());
                PromocionBRL.Delete(int.Parse(gvDatos.Rows[index].Cells[1].Text));
                Response.Redirect("webListaPromocion.aspx");
            }
        }

        protected void btnVolverAlMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("webPrincipal.aspx");
        }

        protected void btnCrearNuevaPromocion_Click(object sender, EventArgs e)
        {
            Response.Redirect("webRegistroPromocion.aspx");
        }
    }
}