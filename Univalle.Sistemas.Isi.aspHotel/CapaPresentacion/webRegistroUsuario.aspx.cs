﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;

namespace CapaPresentacion
{
    public partial class webRegistroUsuario : System.Web.UI.Page
    {
        Usuario usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Usuario.idActual != -1) // si esta logeado, se mostrara la opcion de Logout
                idLogout.InnerText = Usuario.nombreActual + " (Logout)";
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            if(txtNombres.Value!= "" && txtPrimerApellido.Value != "" && txtNombreUsuario.Value != "" && txtContraseña.Value != "" && txtContraseñaRepeticion.Value != "")
            {
                if(txtContraseñaRepeticion.Value.Equals(txtContraseña.Value))
                {
                    try
                    {
                        usuario = new Usuario(txtNombres.Value, txtPrimerApellido.Value, txtSegundoApellido.Value, txtNombreUsuario.Value, txtContraseña.Value, "User");
                        UsuarioBRL.Insert(usuario);
                        if(Usuario.editingdId == -1)
                            Response.Redirect("webLogin.aspx");
                        else
                            Response.Redirect("webListaUsuarios.aspx");
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text = "ERROR " + ex.Message;
                    }
                }
                else
                {
                    lblMensaje.Text = "Las contraseñas no coinciden";
                }
            }
            else
            {
                lblMensaje.Text = "Hay campos vacios";
                if (txtNombres.Value == "" && txtPrimerApellido.Value == "" && txtSegundoApellido.Value == "" && txtNombreUsuario.Value == "" && txtContraseña.Value == "" && txtContraseñaRepeticion.Value == "")
                {
                    if (Usuario.editingdId == -1)
                        Response.Redirect("webLogin.aspx");
                    else
                        Response.Redirect("webListaUsuarios.aspx");
                }
            }
        }
    }
}