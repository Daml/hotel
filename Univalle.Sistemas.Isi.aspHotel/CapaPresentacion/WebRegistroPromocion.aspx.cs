﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using BRL;
using System.IO;

namespace CapaPresentacion
{
    public partial class WebRegistroPromocion : System.Web.UI.Page
    {
        public static string FileName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Si el usuario intenta navegar sin haberse logeado, es redireccionado a la ventana login
            if (Usuario.idActual == -1)
                Response.Redirect("webLogin.aspx");
            else
                idLogout.InnerText = Usuario.nombreActual + " (Logout)";

            if (!IsPostBack)
                imgPromocion.ImageUrl = @"Images/img_default.jpg";
        }
        protected void btnSubir_Click(object sender, EventArgs e)
        {
            if (fuImgPromocion.PostedFile.FileName != "")
            {
                //Guarda el nombre del archivo recuperado
                //FileName = Path.GetFileName(fuImgPromocion.PostedFile.FileName);
                //Parecio mejor idea guardar con la fecha y hora actuales
                FileName = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".png";
                //Save files to images folder
                fuImgPromocion.SaveAs(Server.MapPath("Images/" + FileName));
                imgPromocion.ImageUrl = "Images/" + FileName;
            }
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtTituloPromocion.Text!=string.Empty && txtDescripcion.Text!=string.Empty)
            {
                try
                {
                    Promocion promocion = new Promocion();
                    promocion.Titulo = txtTituloPromocion.Text;
                    promocion.Descripcion = txtDescripcion.Text;
                    promocion.UrlFoto = String.Format("Images/{0}",FileName);
                    PromocionBRL.Insert(promocion);
                    Response.Redirect("webListaPromocion.aspx");
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }

        protected void btnLista_Click(object sender, EventArgs e)
        {
            Response.Redirect("webListaPromocion.aspx");
        }
    }
}