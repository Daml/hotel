﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;

namespace CapaPresentacion
{
    public partial class webLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Si se redirecciona a la ventana login, la sesion se destruye
            Usuario.idActual = -1;
            Usuario.rolActual = "";
            Usuario.nombreActual = "";
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            if (txtNombreUsuario.Value != "" && txtPassword.Value != "")
            {
                if (UsuarioBRL.Login(txtNombreUsuario.Value, txtPassword.Value))
                    Response.Redirect("webPrincipal.aspx");
            }
        }

        protected void ValidatorNombreUsuario_ServerValidate(object source, ServerValidateEventArgs args)
        {

        }

        protected void validUsername_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (txtNombreUsuario.Value == string.Empty)
            {
                validUsername.Text = "Tiene que ingresar un nombre de usuario";
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }

        }

        protected void validPassword_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (txtPassword.Value == string.Empty)
            {
                validPassword.Text = "Tiene que ingresar una contraseña";
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
    }
}