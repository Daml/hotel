﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webEditarUsuario.aspx.cs" EnableEventValidation="false" Inherits="CapaPresentacion.webEditarUsuario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Editar de Usuarios</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>


    <link href="css/bootstrap-grid.css" rel="stylesheet"/>
    <link href="css/bootstrap-grid.min.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.min.css" rel="stylesheet"/>

    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/ai.0.22.9-build00167.js"></script>
    <script src="scripts/ai.0.22.9-build00167.min.js"></script>
    <script src="scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">

            <div class="text-right">
                <a href="webLogin.aspx" id="idLogout" runat="server"></a>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        <form class="form-horizontal" method="post">
                            <fieldset>
                                <legend class="text-center header">Editar usuario</legend>
                                <style>
                                    .header {
                                        color: #36A0FF;
                                        font-size: 27px;
                                        padding: 10px;
                                    }
                                </style>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><!--i class="fa fa-user bigicon"></!i--></span>
                                    <div class="col-md-8">
                                        <input id="txtNombreUsuario" runat="server" name="name" type="text" placeholder="<%$ Resources:Resource, NombreUsuario %>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><!--i class="fa fa-user bigicon"></!i--></span>
                                    <div class="col-md-8">
                                        <input id="txtNombres" runat="server" name="name" type="text" placeholder="<%$ Resources:Resource, Nombres %>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><!--i class="fa fa-user bigicon"></!i--></span>
                                    <div class="col-md-8">
                                        <input id="txtPrimerApellido" runat="server" name="name" type="text" placeholder="<%$ Resources:Resource, PrimerApellido %>" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><!--i class="fa fa-user bigicon"></!i--></span>
                                    <div class="col-md-8">
                                        <input id="txtSegundoApellido" runat="server" name="name" type="text" placeholder="<%$ Resources:Resource, SegundoApellido %>" class="form-control">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"></span>
                                    
                                    <div class="col-md-12 text-center">
                                        <asp:Button ID="btnGuardar" runat="server" Text="<%$ Resources:Resource, Guardar %>" OnClick="btnGuardar_Click"  class="btn btn-primary btn-lg"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"></span>
                                    <div class="col-md-12 text-center">
                                        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
