﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webPrincipal.aspx.cs" Inherits="CapaPresentacion.webPrincipal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Gestion de Usuarios</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>


    <link href="css/bootstrap-grid.css" rel="stylesheet"/>
    <link href="css/bootstrap-grid.min.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.min.css" rel="stylesheet"/>

    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/ai.0.22.9-build00167.js"></script>
    <script src="scripts/ai.0.22.9-build00167.min.js"></script>
    <script src="scripts/jquery-3.3.1.min.js"></script>
</head>
<body> <!--style="background-color: #055970"-->
    <form id="form1" runat="server">
        <main id="main">
            <div class="container">

                <div class="text-right">
                    <a href="webLogin.aspx" id="idLogout" runat="server"></a>
                </div>

              <!-- Page Heading -->
              <h1 class="my-4" style="color: #36A0FF;">HOTEL
                <small>Pagina principal</small>
              </h1>

              <div class="row">
                <div class="col-lg-6 portfolio-item" id="portafolio1" runat="server" visible="false">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" id="imagen1" runat="server" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#" id="titulo1" runat="server"></a>
                      </h4>
                      <p class="card-text"  id="descripcion1" runat="server"></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 portfolio-item" id="portafolio2" runat="server" visible="false">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" id="imagen2" runat="server" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#" id="titulo2" runat="server"></a>
                      </h4>
                      <p class="card-text" id="descripcion2" runat="server"></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 portfolio-item" id="portafolio3" runat="server" visible="false">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" id="imagen3" runat="server" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#" id="titulo3" runat="server"></a>
                      </h4>
                      <p class="card-text" id="descripcion3" runat="server"></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 portfolio-item" id="portafolio4" runat="server" visible="false">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" id="imagen4" runat="server" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#" id="titulo4" runat="server"></a>
                      </h4>
                      <p class="card-text" id="descripcion4" runat="server"></p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.row -->



                <!--
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-header">
                            <Legend class="text-center header" runat="server">Principal</Legend>
                            <style>
                                .header {
                                    color: #36A0FF;
                                    font-size: 27px;
                                    padding: 10px;
                                }
                            </style>
                        </div>
                    </div>
                </div>
                -->

                <div class="row">
                    <div class="col-md-12 text-center">
                        <br/>
                        <asp:Button ID="btnUsuarios" OnClick="btnUsuarios_Click" runat="server" Text="Usuarios"  class="btn btn-danger btn-lg" Width="600px"/>
                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-12 text-center">
                        <br/>
                        <asp:Button ID="btnReservas" OnClick="btnReservas_Click" runat="server" Text="Reservas"  class="btn btn-success btn-lg" Width="600px"/>
                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-12 text-center">
                        <br/>
                        <asp:Button ID="btnHacerReserva" OnClick="btnHacerReserva_Click" runat="server" Text="Realizar reserva"  class="btn btn-primary btn-lg" Width="600px"/>
                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-12 text-center">
                        <br/>
                        <asp:Button ID="btnPromociones" OnClick="btnPromociones_Click" runat="server"  Text="Promociones"  class="btn btn-success btn-lg" Width="600px"/>
                    </div>
                </div>

                <br/>
            </div>
        </main>
    </form>
</body>
</html>
