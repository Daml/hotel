﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BRL;
using Common;

namespace CapaPresentacion
{
    public partial class webListaUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Si el usuario intenta navegar sin haberse logeado, es redireccionado a la ventana login
            if (Usuario.idActual == -1)
                Response.Redirect("webLogin.aspx");
            else
                idLogout.InnerText = Usuario.nombreActual + " (Logout)";

            if (!IsPostBack)
            {
                LoadData();
            }
        }
        
        public void LoadData()
        {
            try
            {
                gvDatos.DataSource = UsuarioBRL.Select().DefaultView;
                gvDatos.DataBind();
                gvDatos.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
                //txtCadena.Value = ex.Message;
            }
        }

        protected void btnCrearNuevoUsuario_Click(object sender, EventArgs e)
        {
            Response.Redirect("webRegistroUsuario.aspx");
        }
        
        protected void gvDatos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EDITAR")
            {
                int index = Convert.ToInt32(e.CommandArgument.ToString());
                Usuario.editingdId = int.Parse(gvDatos.Rows[index].Cells[1].Text);
                Response.Redirect("webEditarUsuario.aspx");
            }
            if (e.CommandName == "ELIMINAR")
            {
                int index = Convert.ToInt32(e.CommandArgument.ToString());
                UsuarioBRL.Delete(int.Parse(gvDatos.Rows[index].Cells[1].Text));
                Response.Redirect("webListaUsuarios.aspx");
            }
            if (e.CommandName == "CAMBIAR")
            {
                int index = Convert.ToInt32(e.CommandArgument.ToString());
                UsuarioBRL.ChangeRol(int.Parse(gvDatos.Rows[index].Cells[1].Text));
                Response.Redirect("webListaUsuarios.aspx");
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                gvDatos.DataSource = UsuarioBRL.SelectLike(txtCadena.Value).DefaultView;
                gvDatos.DataBind();
                gvDatos.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
                //txtCadena.Value = ex.Message;
            }
        }

        protected void btnVolverAlMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("webPrincipal.aspx");
        }
    }
}