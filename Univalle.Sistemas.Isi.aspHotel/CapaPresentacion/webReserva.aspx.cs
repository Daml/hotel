﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;

namespace CapaPresentacion
{
    public partial class webReserva : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Si el usuario intenta navegar sin haberse logeado, es redireccionado a la ventana login
            if(Usuario.idActual == -1)
                Response.Redirect("webLogin.aspx");
            else
                idLogout.InnerText = Usuario.nombreActual + " (Logout)";

            dateFechaIngreso.Value = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
            dateFechaSalida.Value = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;

        }

        protected void btnHacerReserva_Click(object sender, EventArgs e)
        {
            if(txtCantidadCuartos.Value != "" && txtCantidadPersonas.Value != "")
            {
                try
                {
                    Reserva reserva = new Reserva();
                    reserva.CantidadHabitaciones = byte.Parse(txtCantidadCuartos.Value);
                    reserva.CantidadPersonas = byte.Parse(txtCantidadPersonas.Value);
                    reserva.FechaIngreso = DateTime.Parse(dateFechaIngreso.Value);
                    reserva.FechaSalida = DateTime.Parse(dateFechaSalida.Value);
                    ReservaBRL.Insert(reserva);
                    if (Usuario.rolActual == "User")
                        Response.Redirect("webPrincipal.aspx");
                    else
                        Response.Redirect("webListaReservas.aspx");
                }
                catch (Exception)
                {
                }
            }
        }

        protected void btnVolverAlMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("webPrincipal.aspx");
        }
    }
}