﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;

namespace CapaPresentacion
{
    public partial class webPrincipal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Si el usuario intenta navegar sin haberse logeado, es redireccionado a la ventana login
            if (Usuario.idActual == -1)
                Response.Redirect("webLogin.aspx");
            else
                idLogout.InnerText = Usuario.nombreActual + " (Logout)";
            imagen1.Src = @"Images/700x400.png";
            imagen2.Src = @"Images/700x400.png";
            imagen3.Src = @"Images/700x400.png";
            imagen4.Src = @"Images/700x400.png";

            List<Promocion> promociones = PromocionBRL.GetTOP4();
            if(promociones.Count()>0)
            {
                portafolio1.Visible = true;
                titulo1.InnerText = promociones[0].Titulo;
                descripcion1.InnerText = promociones[0].Descripcion;
                imagen1.Src = promociones[0].UrlFoto;
                if (promociones.Count() > 1)
                {
                    portafolio2.Visible = true;
                    titulo2.InnerText = promociones[1].Titulo;
                    descripcion2.InnerText = promociones[1].Descripcion;
                    imagen2.Src = promociones[1].UrlFoto;
                    if (promociones.Count() > 2)
                    {
                        portafolio3.Visible = true;
                        titulo3.InnerText = promociones[2].Titulo;
                        descripcion3.InnerText = promociones[2].Descripcion;
                        imagen3.Src = promociones[2].UrlFoto;
                        if (promociones.Count() > 3)
                        {
                            portafolio4.Visible = true;
                            titulo4.InnerText = promociones[3].Titulo;
                            descripcion4.InnerText = promociones[3].Descripcion;
                            imagen4.Src = promociones[3].UrlFoto;
                        }
                    }
                }
            }
            if(Usuario.rolActual == "User")
            {
                btnPromociones.Visible = false;
                btnUsuarios.Visible = false;
            }
        }

        protected void btnUsuarios_Click(object sender, EventArgs e)
        {

            Response.Redirect("webListaUsuarios.aspx");
        }

        protected void btnReservas_Click(object sender, EventArgs e)
        {

            Response.Redirect("webListaReservas.aspx");
        }

        protected void btnPromociones_Click(object sender, EventArgs e)
        {
            Response.Redirect("webListaPromocion.aspx");
        }

        protected void btnHacerReserva_Click(object sender, EventArgs e)
        {
            Response.Redirect("webReserva.aspx");
        }
    }
}