﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webRegistroPromocion.aspx.cs" Inherits="CapaPresentacion.WebRegistroPromocion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registro Promocion</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>


    <link href="css/bootstrap-grid.css" rel="stylesheet"/>
    <link href="css/bootstrap-grid.min.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.min.css" rel="stylesheet"/>

    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/ai.0.22.9-build00167.js"></script>
    <script src="scripts/ai.0.22.9-build00167.min.js"></script>
    <script src="scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <main id="main">
            <div class="container">

                <div class="text-right">
                    <a href="webLogin.aspx" id="idLogout" runat="server"></a>
                </div>

                <legend class="text-center header">Promocion</legend>
                <style>
                    .header {
                        color: #36A0FF;
                        font-size: 27px;
                        padding: 10px;
                    }
                </style>

              
                <div class="form-row">

                    <div class="form-group col-md-12">
                       <asp:Label runat="server" Text="Titulo Promocion"/>
                        <br />
                        <asp:TextBox runat="server" ID="txtTituloPromocion" Placeholder="Ingrese el titulo de la promocion" T Class="form-control"></asp:TextBox>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <asp:Label runat="server" Text="Descripcion"/>
                        <br />
                        <asp:TextBox runat="server" ID="txtDescripcion" Class="form-control" Placeholder="Ingrese una descripcion" TextMode="MultiLine" Rows="3" />
                    </div>
                    
                    <div class="form-group col-md-12">
                        <div class="text-center">
                            <asp:Label runat="server" Text="Ingrese una imagen"/>
                            <br />
                            <asp:Image runat="server" ID="imgPromocion" class="thumbnail box-center margin-top-20" Width="400" Height="200"/> <!-- Width="300" Height="150"  -->
                        </div>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <div class="text-center"> 
                            <asp:FileUpload ID="fuImgPromocion" CssClass="file-upload" runat="server"/>
                            <asp:Button runat="server" ID="btnSubir" Text="Subir Archivos" CssClass="btn btn-primary" OnClick="btnSubir_Click" />
                        </div>
                    </div>



                    <div class="form-group col-md-12">
                        <div class="text-center">
                            <asp:Button runat="server" ID="btnRegistrar" Text="Registrar" CssClass="btn btn-primary btn-lg" OnClick="btnRegistrar_Click"/>
                        </div>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <div class="text-center">
                            <asp:Button runat="server" ID="btnLista" Text="Ver Lista" CssClass="btn btn-primary btn-lg" OnClick="btnLista_Click" />
                        </div>
                    </div>

                </div>
                
            </div>
        </main>
    </form>
</body>
</html>
