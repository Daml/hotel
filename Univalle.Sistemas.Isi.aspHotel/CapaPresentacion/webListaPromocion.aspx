﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webListaPromocion.aspx.cs" Inherits="CapaPresentacion.webListaPromocion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Lista Promociones</title>
 <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>


    <link href="css/bootstrap-grid.css" rel="stylesheet"/>
    <link href="css/bootstrap-grid.min.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.min.css" rel="stylesheet"/>

    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/ai.0.22.9-build00167.js"></script>
    <script src="scripts/ai.0.22.9-build00167.min.js"></script>
    <script src="scripts/jquery-3.3.1.min.js"></script>
</head>
<body> <!--style="background-color: #055970"-->
    <form id="form1" runat="server">
        <main id="main">
            <div class="container">

                <div class="text-right">
                    <a href="webLogin.aspx" id="idLogout" runat="server"></a>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="section-header">
                            <Legend class="text-center header" runat="server">Promociones</Legend>
                            <style>
                                .header {
                                    color: #36A0FF;
                                    font-size: 27px;
                                    padding: 10px;
                                }
                            </style>
                        </div>
                    </div>
                </div>
                    
                <div class="row">
                        <!-- START table-responsive-->
                        <div class="table-responsive"  style="background-color: #ffffff" >
                            <asp:GridView ID="gvDatos" CssClass="table table-bordered bs-table" runat="server" OnRowCommand="gvDatos_RowCommand">
                                
                                <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Acciones %>">
                                    <ItemTemplate>
                                        <asp:Button ID="btnOcultar" runat="server" CommandName="ELIMINAR" Text="Eliminar" class="btn btn-danger" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    <br/>
                </div>
                
                <br/>
                <div class="row">
                    <div class="col-md-12 text-center">                   
                        <asp:Button ID="btnCrearNuevaPromocion" runat="server" OnClick="btnCrearNuevaPromocion_Click" Text="Crear nueva promocion"  class="btn btn-primary btn-lg"/>
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="col-md-12 text-center">                   
                        <asp:Button ID="btnVolverAlMenu" runat="server" OnClick="btnVolverAlMenu_Click" Text="MENU"  class="btn btn-primary btn-lg"/>
                    </div>
                </div>
                <br/>

            </div>
        </main>
    </form>
</body>
</html>
