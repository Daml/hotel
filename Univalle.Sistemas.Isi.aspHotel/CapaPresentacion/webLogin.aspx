﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webLogin.aspx.cs" Inherits="CapaPresentacion.webLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Gestion de Usuarios</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>


    <link href="css/bootstrap-grid.css" rel="stylesheet"/>
    <link href="css/bootstrap-grid.min.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.min.css" rel="stylesheet"/>
    
    <!-- PURGAR!!!
    <link href="css/animate+animo.css" rel="stylesheet"/>
    <link href="css/csspinner.min.css" rel="stylesheet"/>
    <link href="css/app.css" rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    -->
        
    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/ai.0.22.9-build00167.js"></script>
    <script src="scripts/ai.0.22.9-build00167.min.js"></script>
    <script src="scripts/jquery-3.3.1.min.js"></script>

    <!--
    <script src="scripts/app.js"></script>
    <script src="scripts/animo.min.js"></script>
    <script src="scripts/modernizr.js"></script>
    <script src="scripts/fastclick.js"></script>
    <script src="scripts/bootstrap-slider.js"></script>
    <script src="scripts/jquery.sparkline.min.js"></script>
    <script src="scripts/jquery.slimscroll.min.js"></script>
    <script src="scripts/bootstrap-filestyle.min.js"></script>
    <script src="scripts/chosen.jquery.min.js"></script>
        -->

</head>
<body>
    <form id="form1" runat="server">
        <main id="main">
            <div style="padding: 100px; align-items:center; background-color: #055970" class="row row-table">
                <div style="padding:50px; margin:10px;  background-color: #ffffff" class="col-md-12"> <!-- 8 col-md-offset-2"> -->
                    <p class="text-center mb-lg">
                        <strong>INGRESE SUS DATOS PARA CONTINUAR</strong>
                    </p>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Ingrese su nombre de usuario: </label>
                            <input id="txtNombreUsuario" type="text" runat="server" placeholder="Login" class="form-control" name="login"/>
                    
                            <asp:CustomValidator ID="validUsername"
                            ControlToValidate="txtPassword"
                            OnServerValidate="validUsername_ServerValidate"
                            Text = "Tiene que ingresar un nombre de usuario"
                            runat="server"  ForeColor="#CC0000"/>
                        </div>
                        <div class="form-group">
                            <label>Ingrese su password:</label>
                            <input id="txtPassword" type="password" runat="server" placeholder="Password" class="form-control" name="password"/>
                            <asp:CustomValidator ID="validPassword"
                            ControlToValidate="txtNombreUsuario"
                            OnServerValidate="validPassword_ServerValidate"
                            Text = "Tiene que ingresar una contraseña"
                            runat="server"  ForeColor="#CC0000"/>
                        </div>
                    <asp:Button ID="btnIngresar" runat="server" class="btn btn-block btn-primary" OnClick="btnIngresar_Click" Text="INGRESAR" />
                    <br />
                        <div class="text-center">
                            ¿No tienes una cuenta?<a href="webRegistroUsuario.aspx"> Create una.</a>
                        </div>
                    </div>
                 <!-- END panel-->
              </div>
           </div>
        </main>
    </form>
</body>
</html>
