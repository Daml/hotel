﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webReserva.aspx.cs" Inherits="CapaPresentacion.webReserva" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reservas</title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>


    <link href="css/bootstrap-grid.css" rel="stylesheet"/>
    <link href="css/bootstrap-grid.min.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.css" rel="stylesheet"/>
    <link href="css/bootstrap.reboot.min.css" rel="stylesheet"/>

    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/ai.0.22.9-build00167.js"></script>
    <script src="scripts/ai.0.22.9-build00167.min.js"></script>
    <script src="scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <main id="main">
          <div class="container">


            <div class="text-right">
                <a href="webLogin.aspx" id="idLogout" runat="server"></a>
            </div>

            <legend class="text-center header">Reserva</legend>
            <style>
                .header {
                    color: #36A0FF;
                    font-size: 27px;
                    padding: 10px;
                }
            </style>
        
            <div class="form-row">

                <div class="form-group col-md-12">                    <label for="txtCantidadPersonas">Personas</label>
                    <input id="txtCantidadPersonas" runat="server" type="text" class="form-control" placeholder="Cantidad de personas"/>
                </div>

                <div class="form-group col-md-12">                    <label for="txtCantidadCuartos">Habitaciones</label>
                    <input id="txtCantidadCuartos" runat="server" type="text" class="form-control" placeholder="Cantidad de habitaciones"/>
                </div>


                <div class="form-group col-md-12">                    <label for="fechaIngreso">Fecha ingreso</label>
                    <input id="dateFechaIngreso" runat="server" type="date" step="1" class="form-control" />
                </div>

                <div class="form-group col-md-12">                    <label for="fechaSalida">Fecha salida</label>
                    <input id="dateFechaSalida" runat="server" type="date" step="1" class="form-control" />
                </div>

                <div class="form-group col-md-12" align="text-center">
                    <br/>
                </div>

                <div class="form-group col-md-12" align="text-center">
                    <div class="text-center">
                        <asp:Button ID="btnHacerReserva" OnClick="btnHacerReserva_Click" runat="server" Text="Hacer reserva"  class="btn btn-primary btn-lg"/>
                    </div>
                </div>
              
                <br/>
                <div class="form-group col-md-12" align="text-center">
                    <div class="text-center">                   
                        <asp:Button ID="btnVolverAlMenu" runat="server" OnClick="btnVolverAlMenu_Click" Text="MENU"  class="btn btn-primary btn-lg"/>
                    </div>
                </div>
                <br/>

            </div>
          </main>
    </form>
</body>
</html>
