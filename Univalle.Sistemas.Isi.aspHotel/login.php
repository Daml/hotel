  <title>Ginas's Hotel Login</title>
</head>

<body id="body">
   <div style="height: 100%; padding: 50px 0; background-color: #055970" class="row row-table">
      <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12 align-middle">
         <div data-toggle="play-animation" data-play="fadeInUp" data-offset="0" class="panel panel-default panel-flat">
            <br>
            <p class="text-center mb-lg">
               <strong>INGRESE SUS DATOS PARA CONTINUAR</strong>
            </p>
            <div class="panel-body">
               <form role="form" action="<?php echo base_url(); ?>index.php/hotel/validarusr" method="post" class="mb-lg">

                  <div class="form-group">
                    <label>Ingrese su login: </label>
                    <input type="text" placeholder="Login" class="form-control" name="login">
                  </div>

                  <div class="form-group">
                    <label>Ingrese su password:</label>
                    <input type="password" placeholder="Password" class="form-control" name="password">
                  </div>
                  <button type="submit" class="btn btn-block btn-primary">INGRESAR</button>
               </form>
            </div>
         </div>
         <!-- END panel-->
      </div>
   </div>