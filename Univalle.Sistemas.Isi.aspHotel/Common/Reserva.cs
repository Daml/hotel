﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Reserva
    {
        private int idReserva;
        private DateTime fechaIngreso;
        private DateTime fechaSalida;
        private byte cantidadPersonas;
        private byte cantidadHabitaciones;
        private int idPersona;

        public Reserva ()
        {

        }
        public Reserva (int idReserva, DateTime fechaIngreso, DateTime fechaSalida, byte cantidadPersonas, byte cantidadHabitaciones, int idPersona)
        {
            this.idReserva = idReserva;
            this.fechaIngreso = fechaIngreso;
            this.fechaSalida = fechaSalida;
            this.cantidadPersonas = cantidadPersonas;
            this.cantidadHabitaciones = cantidadHabitaciones;
            this.idPersona = idPersona;
        }

        public int IdReserva
        {
            get
            {
                return idReserva;
            }

            set
            {
                idReserva = value;
            }
        }

        public DateTime FechaIngreso
        {
            get
            {
                return fechaIngreso;
            }

            set
            {
                fechaIngreso = value;
            }
        }

        public DateTime FechaSalida
        {
            get
            {
                return fechaSalida;
            }

            set
            {
                fechaSalida = value;
            }
        }

        public byte CantidadPersonas
        {
            get
            {
                return cantidadPersonas;
            }

            set
            {
                cantidadPersonas = value;
            }
        }

        public byte CantidadHabitaciones
        {
            get
            {
                return cantidadHabitaciones;
            }

            set
            {
                cantidadHabitaciones = value;
            }
        }

        public int IdPersona
        {
            get
            {
                return idPersona;
            }

            set
            {
                idPersona = value;
            }
        }
    }
}
