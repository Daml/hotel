﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class Usuario : Persona
    {
        private int idUsuario;
        private string nombreUsuario;
        private string password;
        private string rol;
        public static int editingdId = -1;

        public static int idActual = -1;//id del usuario de la sesion actual
        public static string rolActual = string.Empty; // rol del usuario de la sesion actual
        public static string nombreActual = string.Empty; // nombre del usuario de la sesion actual

        public Usuario()
        {

        }
        public Usuario(string nombres, string primerApellido, string segundoApellido, string nombreUsuario, string password, string rol)
        {
            this.Nombres = nombres;
            this.PrimerApellido = primerApellido;
            this.SegundoApellido = segundoApellido;
            this.NombreUsuario = nombreUsuario;
            this.Password = password;
            this.Rol = rol;
        }
        public Usuario(int idUsuario, string nombreUsuario, string nombres, string primerApellido, string segundoApellido)
        {
            this.IdUsuario = idUsuario;
            this.NombreUsuario = nombreUsuario;
            IdPersona = idUsuario;
            Nombres = nombres;
            PrimerApellido = primerApellido;
            SegundoApellido = segundoApellido;
        }
        public int IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }

        public string NombreUsuario
        {
            get
            {
                return nombreUsuario;
            }

            set
            {
                nombreUsuario = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string Rol
        {
            get
            {
                return rol;
            }

            set
            {
                rol = value;
            }
        }
    }
}