﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class Promocion
    {
        private int idPromocion;
        private string titulo;
        private string descripcion;
        private string urlFoto;
        private string urlVideo;
        private string idUsuario;

        public Promocion()
        {

        }

        public Promocion(int idPromocion, string titulo, string descripcion, string urlFoto, string urlVideo, string idUsuario)
        {
            this.IdPromocion = idPromocion;
            this.Titulo = titulo;
            this.Descripcion = descripcion;
            this.UrlFoto = urlFoto;
            this.UrlVideo = urlVideo;
            this.IdUsuario = idUsuario;
        }

        public Promocion(string titulo, string descripcion, string urlFoto)
        {
            this.Titulo = titulo;
            this.Descripcion = descripcion;
            this.UrlFoto = urlFoto;
        }
        public int IdPromocion
        {
            get
            {
                return idPromocion;
            }

            set
            {
                idPromocion = value;
            }
        }

        public string Titulo
        {
            get
            {
                return titulo;
            }

            set
            {
                titulo = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public string UrlFoto
        {
            get
            {
                return urlFoto;
            }

            set
            {
                urlFoto = value;
            }
        }

        public string UrlVideo
        {
            get
            {
                return urlVideo;
            }

            set
            {
                urlVideo = value;
            }
        }

        public string IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }
    }
}