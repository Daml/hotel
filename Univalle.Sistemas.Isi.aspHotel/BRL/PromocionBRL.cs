﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using Common;

namespace BRL
{
    public static class PromocionBRL
    {
        public static void Insert(Promocion promocion)
        {
            PromocionDAL.Insert(promocion);
        }

        public static void Update(Promocion promocion)
        {
            PromocionDAL.Update(promocion);
        }

        public static void Delete(int idPromocion)
        {
            PromocionDAL.Delete(idPromocion);
        }

        public static DataTable Select()
        {
            return PromocionDAL.Select();
        }

        public static List<Promocion> GetTOP4()
        {
            return PromocionDAL.GetTOP4();
        }
    }
}
