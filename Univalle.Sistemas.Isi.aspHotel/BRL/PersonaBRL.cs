﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using Common;

namespace BRL
{
    public class PersonaBRL
    {
        public static void Insert(Persona pers)
        {
            PersonaDAL.Insert(pers);
        }

        public static void Update(Persona pers)
        {
            PersonaDAL.Update(pers);
        }

        public static void Delete(int idPersona)
        {
            PersonaDAL.Delete(idPersona);
        }

        public static DataTable Select()
        {
            return PersonaDAL.Select();
        }
    }
}
