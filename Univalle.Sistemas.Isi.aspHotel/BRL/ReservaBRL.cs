﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using Common;

namespace BRL
{
    public static class ReservaBRL
    {
        public static void Insert(Reserva reserva)
        {
            ReservaDAL.Insert(reserva);
        }

        public static void Update(Reserva reserva)
        {
            ReservaDAL.Update(reserva);
        }

        public static void Delete(int idReserva)
        {
            ReservaDAL.Delete(idReserva);
        }

        public static DataTable Select()
        {
            return ReservaDAL.Select();
        }
        public static DataTable SelectByUserId(int idUsuario)
        {
            return ReservaDAL.SelectByUserId(idUsuario);
        }
    }
}
