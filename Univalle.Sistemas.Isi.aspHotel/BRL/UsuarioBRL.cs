﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Common;
using System.Data;

namespace BRL
{
    public static class UsuarioBRL
    {
        public static void Insert(Usuario usuario)
        {
            UsuarioDAL.Insert(usuario);
        }

        public static void Update(Usuario usuario)
        {
            UsuarioDAL.Update(usuario);
        }

        public static void Delete(int idUsuario)
        {
            UsuarioDAL.Delete(idUsuario);
        }

        public static void ChangeRol(int idUsuario)
        {
            UsuarioDAL.ChangeRol(idUsuario);
        }

        public static DataTable SelectLike(string cadena)
        {
            return UsuarioDAL.SelectLike(cadena);
        }

        public static DataTable Select()
        {
            return UsuarioDAL.Select();
        }
        

        public static Usuario getUsuario(int idUsuario)
        {
            return UsuarioDAL.GetUsuario(idUsuario);
        }


        public static bool Login(string nombreUsuario, string password)
        {
            return UsuarioDAL.Login(nombreUsuario, password);
        }
    }
}
