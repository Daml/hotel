﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public static class ReservaDAL
    {
        public static void Insert(Reserva reserva)
        {
            string query = "INSERT INTO Reserva (fechaIngreso, fechaSalida, cantidadPersonas, cantidadHabitaciones, idUsuario) VALUES (@fechaIngreso, @fechaSalida, @cantidadPersonas, @cantidadHabitaciones, @idUsuario)";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@fechaIngreso", reserva.FechaIngreso);
                cmd.Parameters.AddWithValue("@fechaSalida", reserva.FechaSalida);
                cmd.Parameters.AddWithValue("@cantidadPersonas", reserva.CantidadPersonas);
                cmd.Parameters.AddWithValue("@cantidadHabitaciones", reserva.CantidadHabitaciones);
                cmd.Parameters.AddWithValue("@idUsuario", Usuario.idActual);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("ReservaDAL", "INSERCION EXITOSA", "");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("ReservaDAL", "ERROR INSERCION", ex.Message);
            }
        }

        public static void Update(Reserva reserva)
        {
            string query = "UPDATE Reserva SET fechaIngreso = @fechaIngreso, fechaSalida = @fechaSalida, cantidadPersonas = @cantidadPersonas, cantidadHabitaciones = @cantidadHabitaciones WHERE idReserva = @idReserva";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@fechaIngreso", reserva.FechaIngreso);
                cmd.Parameters.AddWithValue("@fechaSalida", reserva.FechaSalida);
                cmd.Parameters.AddWithValue("@cantidadPersonas", reserva.CantidadPersonas);
                cmd.Parameters.AddWithValue("@cantidadHabitaciones", reserva.CantidadHabitaciones);
                cmd.Parameters.AddWithValue("@idReserva", reserva.IdReserva);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("ReservaDAL", "ACTUALIZACION EXITOSA", "");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("ReservaDAL", "ERROR ACTUALIZACION", ex.Message);
            }
        }

        public static void Delete(int idReserva)
        {
            SqlCommand cmd = null;
            string query = "UPDATE Reserva SET estado = 0 WHERE idReserva = @idReserva";
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idReserva", idReserva);
                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                Methods.GenerateLogs("UsuarioDAL", "ERROR Delete", ex.Message);
            }
        }

        public static DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwReservasSelect ORDER BY 2";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public static DataTable SelectByUserId(int idUsuario)
        {
            DataTable res = new DataTable();
            string query = @"SELECT R.idReserva, CONCAT(P.nombres, ' ', P.primerApellido, ' ', P.segundoApellido) AS [Nombre huesped], CONVERT(VARCHAR(10), R.fechaIngreso, 120) AS [Fecha ingreso], CONVERT(VARCHAR(10), 
                            R.fechaSalida, 120) AS [Fecha salida], R.cantidadPersonas AS [Cantidad de personas], R.cantidadHabitaciones AS [Cantidad de habitaciones]
                            FROM Reserva R INNER JOIN Usuario AS U ON U.idUsuario = R.idUsuario 
                            INNER JOIN Persona P ON P.idPersona = U.idUsuario
                            WHERE (R.estado = 1) AND (R.idUsuario = @idUsuario)";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                res = Methods.ExecuteDataTableCommand(cmd);
                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
    }
}
