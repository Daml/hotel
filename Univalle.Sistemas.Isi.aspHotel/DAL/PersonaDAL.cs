﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Common;

namespace DAL
{
    public static class PersonaDAL
    {
        public static void Insert(Persona pers)
        {
            string query = "INSERT INTO Persona (nombres,primerApellido,segundoApellido)  VALUES (@ci,@nombres,@primerApellido,@segundoApellido,@fechaNacimiento,@telefono,@idUsuario)";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);

                cmd.Parameters.AddWithValue("@nombres", pers.Nombres);
                cmd.Parameters.AddWithValue("@primerApellido", pers.PrimerApellido);
                cmd.Parameters.AddWithValue("@segundoApellido", pers.SegundoApellido);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("PersonaDAL", "INSERCION EXITOSA", "");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("PersonaDAL", "ERROR INSERCION", ex.Message);
            }
        }

        public static void Update(Persona pers)
        {
            string query = "UPDATE Persona SET nombres = @nombres, primerApellido = @primerApellido, segundoApellido = @segundoApellido WHERE idPersona = @idPersona";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPersona", pers.IdPersona);
                cmd.Parameters.AddWithValue("@nombres", pers.Nombres);
                cmd.Parameters.AddWithValue("@primerApellido", pers.PrimerApellido);
                cmd.Parameters.AddWithValue("@segundoApellido", pers.SegundoApellido);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("PersonaDAL", "ACTUALIZACION EXITOSA", "");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("PersonaDAL", "ERROR ACTUALIZACION", ex.Message);
            }
        }

        public static void Delete(int idPersona)
        {
            string query = "DELETE FROM Persona WHERE idPersona = @idPersona";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPersona", idPersona);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("PersonaDAL", "ELIMINACION EXITOSA", "");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("PersonaDAL", "ERROR ELIMINACION", ex.Message);
            }
        }

        public static DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT idPersona, nombres, primerApellido, segundoApellido FROM Persona ORDER BY 1";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
    }
}
