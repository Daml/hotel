﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public static class UsuarioDAL
    {
        public static void Insert(Usuario usuario)
        {
            string query1 = "INSERT INTO Persona (nombres,primerApellido,segundoApellido)  VALUES (@nombres,@primerApellido,@segundoApellido)";
            string query2 = "INSERT INTO Usuario (idUsuario,nombreUsuario,password,rol) VALUES (@idUsuario,@nombreUsuario,HASHBYTES('md5', @password),@rol)";
            List<SqlCommand> cmds = new List<SqlCommand>();
            try
            {
                cmds = Methods.CreateNBasicCommands(2);
                cmds[0].CommandText = query1;
                cmds[1].CommandText = query2;

                cmds[0].Parameters.AddWithValue("@nombres", usuario.Nombres);
                cmds[0].Parameters.AddWithValue("@primerApellido", usuario.PrimerApellido);
                cmds[0].Parameters.AddWithValue("@segundoApellido", usuario.SegundoApellido);

                int id = Methods.GetCurrentValueIDTable("Persona");

                cmds[1].Parameters.AddWithValue("@idUsuario", id);
                cmds[1].Parameters.AddWithValue("@nombreUsuario", usuario.NombreUsuario);
                cmds[1].Parameters.AddWithValue("@password", usuario.Password).SqlDbType = SqlDbType.VarChar;
                cmds[1].Parameters.AddWithValue("@rol", usuario.Rol);

                Methods.Execute2BasicCommand(cmds[0], cmds[1]);

                Methods.GenerateLogs("UsuarioDAL", "Usuario insertado", "Insert exitoso");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("UsuarioDAL", "ERROR Insert", ex.Message);
            }
        }

        public static void Update(Usuario usuario)
        {
            string query1 = "UPDATE Persona SET nombres = @nombres, primerApellido = @primerApellido, segundoApellido = @segundoApellido WHERE idPersona = @idUsuario";
            string query2 = "UPDATE Usuario SET nombreUsuario = @nombreUsuario WHERE idUsuario = @idUsuario";
            List<SqlCommand> cmds = new List<SqlCommand>();
            try
            {
                cmds = Methods.CreateNBasicCommands(2);
                cmds[0].CommandText = query1;
                cmds[1].CommandText = query2;


                cmds[0].Parameters.AddWithValue("@idUsuario", usuario.IdUsuario);
                cmds[0].Parameters.AddWithValue("@nombres", usuario.Nombres);
                cmds[0].Parameters.AddWithValue("@primerApellido", usuario.PrimerApellido);
                cmds[0].Parameters.AddWithValue("@segundoApellido", usuario.SegundoApellido);

                cmds[1].Parameters.AddWithValue("@idUsuario", usuario.IdUsuario);
                cmds[1].Parameters.AddWithValue("@nombreUsuario", usuario.NombreUsuario);

                Methods.Execute2BasicCommand(cmds[0], cmds[1]);

                Methods.GenerateLogs("UsuarioDAL", "Update", "Usuario actualizado con id: " + usuario.IdUsuario);
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("UsuarioDAL", "ERROR Update", ex.Message);
            }
        }

        public static void Delete(int idUsuario)
        {
            SqlCommand cmd = null;
            string query = "UPDATE Usuario SET estado = 0 WHERE idUsuario = @idUsuario";
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                Methods.GenerateLogs("UsuarioDAL", "ERROR Delete", ex.Message);
            }
        }

        public static void ChangeRol(int idUsuario)
        {
            SqlCommand cmd = null;
            string query = @"   UPDATE Usuario
                                SET rol = CASE
                                WHEN rol = 'User' THEN 'Admin'
                                ELSE 'User'
                                END
                                WHERE idUsuario = @idUsuario";
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("UsuarioDAL", "ChangeRol", "Se cambio de rol al usuario " + idUsuario);
            }
            catch (Exception ex)
            {
                Methods.GenerateLogs("UsuarioDAL", "Error cambiando rol al usuario " +idUsuario, ex.Message);
            }
        }

        public static DataTable SelectLike(string cadena)
        {
            DataTable res = new DataTable();
            string query = @"SELECT U.idUsuario, U.nombreUsuario AS Usuario, CONCAT(P.nombres, ' ', P.primerApellido, ' ', P.segundoApellido) AS [Nombre Completo], U.rol AS Rol
                            FROM dbo.Usuario AS U 
                            INNER JOIN dbo.Persona AS P ON P.idPersona = U.idUsuario
                            WHERE(U.estado = 1) AND (U.nombreUsuario LIKE CONCAT('%',@cadena,'%') OR P.nombres LIKE CONCAT('%',@cadena,'%') OR P.primerApellido LIKE CONCAT('%',@cadena,'%') OR P.segundoApellido LIKE CONCAT('%',@cadena,'%'))";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@cadena", cadena);
                res = Methods.ExecuteDataTableCommand(cmd);
                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }


        public static DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwUsuarioSelect ORDER BY 2";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        public static Usuario GetUsuario(int idUsuario)
        {
            Usuario res = null;
            SqlDataReader dr = null;
            SqlCommand cmd = null;
            string query = @"SELECT U.idUsuario, U.nombreUsuario, P.nombres, P.primerApellido, P.segundoApellido
                                FROM Usuario U
                                INNER JOIN Persona P ON P.idPersona = U.idUsuario
                                WHERE U.idUsuario = @idUsuario";
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                dr = Methods.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Usuario(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString());
                }
            }
            catch (Exception ex)
            {

                Methods.GenerateLogs("UsuarioDAL ", "ERROR ", ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }

        public static bool Login(string nombreUsuario, string password)
        {
            SqlDataReader dr = null;
            SqlCommand cmd = null;
            //string query = @"SELECT idUsuario, rol FROM Usuario WHERE nombreUsuario = @nombreUsuario AND password = HASHBYTES('md5', @password)";
            string query = @"   SELECT U.idUsuario, U.rol, CONCAT(P.nombres,' ',P.primerApellido)
                                FROM Usuario U
                                INNER JOIN Persona P ON P.idPersona = U.idUsuario
                                WHERE U.nombreUsuario = @nombreUsuario AND U.password = HASHBYTES('md5', @password)";
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", nombreUsuario);
                cmd.Parameters.AddWithValue("@password", password).SqlDbType = SqlDbType.VarChar;
                dr = Methods.ExecuteDataReaderCommand(cmd);

                Methods.GenerateLogs("UsuarioDAL", "LOGIN DE USUARIOS","Nombre Usuario: "+nombreUsuario);
                while (dr.Read())
                {
                    Usuario.idActual = int.Parse(dr[0].ToString());
                    Usuario.rolActual = dr[1].ToString();
                    Usuario.nombreActual = dr[2].ToString();
                    return true;
                }
            }
            catch (Exception ex)
            {

                Methods.GenerateLogs("UsuarioDAL ", "ERROR LOGIN", ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return false;
        }
    }
}
