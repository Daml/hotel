﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public sealed class Methods
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["CapaPresentacion.Properties.Settings.BDDHotelConnectionString"].ConnectionString;

        public static int GetCurrentValueIDTable(string tabla)
        {
            int res = -1;
            string query = "SELECT IDENT_CURRENT('" + tabla + "') + IDENT_INCR('" + tabla + "')";
            try
            {
                SqlCommand cmd = CreateBasicCommand(query);
                res = int.Parse(ExecuteScalarCommand(cmd));
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return res;
        }

        /// <summary>
        /// Retorna un SqlCommand relacionado a la conexion
        /// </summary>
        /// <returns></returns>
        public static SqlCommand CreateBasicCommand()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            return cmd;
        }
        public static SqlCommand CreateBasicCommand(string query)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = connection;
            return cmd;
        }

        public static List<SqlCommand> CreateNBasicCommands(int n)
        {
            List<SqlCommand> res = new List<SqlCommand>();
            SqlConnection connection = new SqlConnection(connectionString);
            for (int i = 0; i < n; i++)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                res.Add(cmd);
            }
            return res;
        }
        /// <summary>
        /// Recibe un SqlCommand y lo ejecuta con un ExecuteNonQuery
        /// </summary>
        /// <param name="cmd"></param>
        public static void ExecuteBasicCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public static void ExecuteBasicCommand(SqlCommand cmd, string query)
        {
            try
            {
                cmd.CommandText = query;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public static void Execute2BasicCommand(SqlCommand cmd1, SqlCommand cmd2)
        {
            SqlTransaction tran = null;

            try
            {
                cmd1.Connection.Open();
                tran = cmd1.Connection.BeginTransaction();

                cmd1.Transaction = tran;
                cmd1.ExecuteNonQuery();

                cmd2.Transaction = tran;
                cmd2.ExecuteNonQuery();

                //Si llegamoks aqui todo OK
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                cmd1.Connection.Close();
            }

        }

        public static void ExecuteNBasicCommand(List<SqlCommand> cmds)
        {
            SqlTransaction tran = null;
            SqlCommand cmd1 = cmds[0];

            try
            {
                cmd1.Connection.Open();
                tran = cmd1.Connection.BeginTransaction();

                foreach (SqlCommand item in cmds)
                {
                    item.Transaction = tran;
                    item.ExecuteNonQuery();
                }

                //Si llegamoks aqui todo OK
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                cmd1.Connection.Close();
            }

        }

        public static string ExecuteScalarCommand(SqlCommand cmd)
        {

            try
            {
                cmd.Connection.Open();
                return cmd.ExecuteScalar().ToString();

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();

            }

        }

        public static SqlDataReader ExecuteDataReaderCommand(SqlCommand cmd)
        {
            SqlDataReader res = null;
            try
            {
                cmd.Connection.Open();
                res = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public static DataTable ExecuteDataTableCommand(SqlCommand cmd)
        {
            DataTable res = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(res);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }

        public static int ValidateName(string name, SqlCommand cmd)
        {
            int res = 0;
            DataTable data = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(data);
                res = data.Rows.Count;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }

        public static void GenerateLogs(string clase, string metodo, string mensaje)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} {2} {3}", DateTime.Now, mensaje,  metodo, clase));
        }
    }
}
