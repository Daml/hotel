﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public static class PromocionDAL
    {
        public static void Insert(Promocion promocion)
        {
            string query = "INSERT INTO Promocion (titulo,descripcion,urlFoto,idUsuario)  VALUES (@Titulo,@Descripcion,@UrlFoto,@idUsuario)";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);

                cmd.Parameters.AddWithValue("@Titulo", promocion.Titulo);
                cmd.Parameters.AddWithValue("@Descripcion", promocion.Descripcion);
                cmd.Parameters.AddWithValue("@UrlFoto", promocion.UrlFoto);
                cmd.Parameters.AddWithValue("@idUsuario", Usuario.idActual);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("PromocionDAL", "INSERCION EXITOSA", "");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("PromocionDAL", "ERROR INSERCION", ex.Message);
            }
        }

        public static void Update(Promocion promocion)
        {
            string query = "UPDATE Promocion SET titulo = @Titulo, descripcion = @Descripcion, urlFoto = @UrlFoto, idUsuario = @IdUsuario WHERE idPromocion = @IdPromocion";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@IdPromocion", promocion.IdPromocion);
                cmd.Parameters.AddWithValue("@Titulo", promocion.Titulo);
                cmd.Parameters.AddWithValue("@Descripcion", promocion.Descripcion);
                cmd.Parameters.AddWithValue("@UrlFoto", promocion.UrlFoto);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("PromocionDAL", "ACTUALIZACION EXITOSA", "");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("PromocionDAL", "ERROR ACTUALIZACION", ex.Message);
            }
        }

        public static void Delete(int idPromocion)
        {
            string query = "UPDATE Promocion SET estado = 0 WHERE idPromocion = @idPromocion";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPromocion", idPromocion);
                Methods.ExecuteBasicCommand(cmd);
                Methods.GenerateLogs("PromocionDAL", "ELIMINACION EXITOSA", "");
            }
            catch (Exception ex)
            {
                //Generar log 
                Methods.GenerateLogs("PromocionDAL", "ERROR ELIMINACION", ex.Message);
            }
        }

        public static DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwPromocion ORDER BY 2";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public static List<Promocion> GetTOP4()
        {
            List<Promocion> res = new List<Promocion>();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT TOP 4 titulo, descripcion, urlFoto FROM Promocion WHERE estado = 1";
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                dr = Methods.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res.Add(new Promocion(dr[0].ToString(), dr[1].ToString(), dr[2].ToString()));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
    }
}
